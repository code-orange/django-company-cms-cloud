from django.utils.translation import gettext as _


def cloud_fileshare_product_comparison():
    available_products = list()

    # HSFS1000G
    available_products.append(
        {
            "name": _("Hosted FileShare 1 Terabyte Upgrade"),
            "price": "49.99 Euro" + " " + _("(monthly)"),
            "text": _(
                "1 Terabyte (1.000 Gigabyte) of additional space for your files."
            ),
        }
    )

    # HSFS500G
    available_products.append(
        {
            "name": _("Hosted FileShare 500 Gigabyte Upgrade"),
            "price": "29.99 Euro" + " " + _("(monthly)"),
            "text": _("500 Gigabyte of additional space for your files."),
        }
    )

    # HSFS100G
    available_products.append(
        {
            "name": _("Hosted FileShare 100 Gigabyte Upgrade"),
            "price": "9.99 Euro" + " " + _("(monthly)"),
            "text": _("100 Gigabyte of additional space for your files."),
        }
    )

    return available_products


def cloud_online_backup_product_comparison():
    available_products = list()

    # HSOBSRV
    available_products.append(
        {
            "name": _("Hosted Online Backup Server"),
            "price": "29.99 Euro" + " " + _("(monthly)"),
            "text": _(
                "Online Backup Service for one server. Includes 200 Gigabyte of backup storage (pooling)."
            ),
        }
    )

    # HSOBWKS
    available_products.append(
        {
            "name": _("Hosted Online Backup Workstation"),
            "price": "19.99 Euro" + " " + _("(monthly)"),
            "text": _(
                "Online Backup Service for one workstation. Includes 200 Gigabyte of backup storage (pooling)."
            ),
        }
    )

    # HSOB300G
    available_products.append(
        {
            "name": _("Hosted Online Backup 300 Gigabyte Storage"),
            "price": "29.99 Euro" + " " + _("(monthly)"),
            "text": _("Additional 300 Gigabyte of backup storage (pooling)."),
        }
    )

    return available_products
