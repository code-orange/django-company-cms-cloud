from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="cloud"),
    path("dedicated-server", views.dedicated_server, name="cloud_dedicated_server"),
    path("fileshare", views.fileshare, name="cloud_fileshare"),
    path(
        "identity-and-access-management",
        views.identity_and_access_management,
        name="cloud_identity_and_access_management",
    ),
    path(
        "internet-of-things", views.internet_of_things, name="cloud_internet_of_things"
    ),
    path("network-security", views.network_security, name="cloud_network_security"),
    path("online-backup", views.online_backup, name="cloud_online_backup"),
    path(
        "virtual-desktop-infrastructure",
        views.virtual_desktop_infrastructure,
        name="cloud_virtual_desktop_infrastructure",
    ),
    path("virtual-server", views.virtual_server, name="cloud_virtual_server"),
    path(
        "network-management", views.network_management, name="cloud_network_management"
    ),
]
