from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class CloudStaticViewSitemap(Sitemap):
    def items(self):
        return [
            "cloud",
            "cloud_dedicated_server",
            "cloud_fileshare",
            "cloud_identity_and_access_management",
            "cloud_internet_of_things",
            "cloud_network_security",
            "cloud_online_backup",
            "cloud_virtual_desktop_infrastructure",
            "cloud_virtual_server",
            "cloud_network_management",
        ]

    def location(self, item):
        return reverse(item)
