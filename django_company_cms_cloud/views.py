from math import ceil

from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_cloud.django_company_cms_cloud.products import *
from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)


def home(request):
    template = loader.get_template("django_company_cms_cloud/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("What is that?")

    template_opts["product_cloud_home_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_cloud_home_01_header"
    ).content

    template_opts["product_cloud_home_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_cloud_home_01_content"
    ).content

    template_opts["product_cloud_home_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_cloud_home_02_header"
    ).content

    template_opts["product_cloud_home_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_cloud_home_02_content"
    ).content

    product_list = list()

    cloud_home_product_filter_1 = "product_cloud_home_item_"
    cloud_home_product_filter_2 = "_name"

    cloud_home_products = CompanyCmsGeneral.objects.filter(
        name__startswith=cloud_home_product_filter_1,
        name__endswith=cloud_home_product_filter_2,
    )

    for cloud_home_product in cloud_home_products:
        key = cloud_home_product.name[
            : len(cloud_home_product.name) - len(cloud_home_product_filter_2)
        ]

        product_list.append(
            {
                "name": CompanyCmsGeneral.objects.get(name=key + "_name").content,
                "subtitle": CompanyCmsGeneral.objects.get(
                    name=key + "_subtitle"
                ).content,
                "icon": CompanyCmsGeneral.objects.get(name=key + "_icon").content,
            }
        )

    template_opts["product_list"] = product_list

    return HttpResponse(template.render(template_opts, request))


def dedicated_server(request):
    template = loader.get_template("django_company_cms_cloud/dedicated_server.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Dedicated Server")

    template_opts["product_dedicated_server_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_dedicated_server_01_header"
    ).content

    template_opts["product_dedicated_server_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_dedicated_server_01_content"
        ).content
    )

    template_opts["product_dedicated_server_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_dedicated_server_02_header"
    ).content

    template_opts["product_dedicated_server_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_dedicated_server_02_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def fileshare(request):
    template = loader.get_template("django_company_cms_cloud/fileshare.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Fileshare")

    template_opts["product_fileshare_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_fileshare_01_header"
    ).content

    template_opts["product_fileshare_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_fileshare_01_content"
    ).content

    template_opts["product_fileshare_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_fileshare_02_header"
    ).content

    template_opts["product_fileshare_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_fileshare_02_content"
    ).content

    template_opts["product_fileshare_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_fileshare_03_header"
    ).content

    template_opts["product_fileshare_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_fileshare_03_content"
    ).content

    available_products = cloud_fileshare_product_comparison()

    template_opts["product_comparison_title"] = _("Storage extension is inexpensive!")
    template_opts["product_comparison_subtitle"] = _(
        "You can always upgrade to receive additional storage space."
    )

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))


def identity_and_access_management(request):
    template = loader.get_template(
        "django_company_cms_cloud/identity_and_access_management.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Identity & Access Management")

    template_opts["product_identity_and_access_management_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_identity_and_access_management_01_header"
        ).content
    )

    template_opts["product_identity_and_access_management_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_identity_and_access_management_01_content"
        ).content
    )

    template_opts["product_identity_and_access_management_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_identity_and_access_management_02_header"
        ).content
    )

    template_opts["product_identity_and_access_management_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_identity_and_access_management_02_content"
        ).content
    )

    template_opts["product_identity_and_access_management_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_identity_and_access_management_03_header"
        ).content
    )

    template_opts["product_identity_and_access_management_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_identity_and_access_management_03_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def internet_of_things(request):
    template = loader.get_template("django_company_cms_cloud/internet_of_things.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("IoT - Internet of Things")

    template_opts["product_internet_of_things_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_internet_of_things_01_header"
        ).content
    )

    template_opts["product_internet_of_things_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_internet_of_things_01_content"
        ).content
    )

    template_opts["product_internet_of_things_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_internet_of_things_02_header"
        ).content
    )

    template_opts["product_internet_of_things_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_internet_of_things_02_content"
        ).content
    )

    template_opts["product_internet_of_things_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_internet_of_things_03_header"
        ).content
    )

    template_opts["product_internet_of_things_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_internet_of_things_03_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def network_security(request):
    template = loader.get_template("django_company_cms_cloud/network_security.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Network Protection")

    template_opts["product_network_security_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_network_security_01_header"
    ).content

    template_opts["product_network_security_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_security_01_content"
        ).content
    )

    template_opts["product_network_security_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_network_security_02_header"
    ).content

    template_opts["product_network_security_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_security_02_content"
        ).content
    )

    template_opts["product_network_security_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_network_security_03_header"
    ).content

    template_opts["product_network_security_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_security_03_content"
        ).content
    )

    template_opts["product_network_security_04_header"] = CompanyCmsGeneral.objects.get(
        name="product_network_security_04_header"
    ).content

    template_opts["product_network_security_04_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_security_04_content"
        ).content
    )

    template_opts["product_network_security_05_header"] = CompanyCmsGeneral.objects.get(
        name="product_network_security_05_header"
    ).content

    template_opts["product_network_security_05_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_security_05_content"
        ).content
    )

    template_opts["product_network_security_06_header"] = CompanyCmsGeneral.objects.get(
        name="product_network_security_06_header"
    ).content

    template_opts["product_network_security_06_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_security_06_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def online_backup(request):
    template = loader.get_template("django_company_cms_cloud/online_backup.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Online Backup")

    template_opts["product_online_backup_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_01_header"
    ).content

    template_opts["product_online_backup_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_01_content"
    ).content

    template_opts["product_online_backup_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_02_header"
    ).content

    template_opts["product_online_backup_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_02_content"
    ).content

    template_opts["product_online_backup_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_03_header"
    ).content

    template_opts["product_online_backup_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_03_content"
    ).content

    template_opts["product_online_backup_04_header"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_04_header"
    ).content

    template_opts["product_online_backup_04_content"] = CompanyCmsGeneral.objects.get(
        name="product_online_backup_04_content"
    ).content

    available_products = cloud_online_backup_product_comparison()

    template_opts["product_comparison_title"] = _("Our Online Backup Services")
    template_opts["product_comparison_subtitle"] = _(
        "You can always add additional licenses or storage space."
    )

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))


def virtual_desktop_infrastructure(request):
    template = loader.get_template(
        "django_company_cms_cloud/virtual_desktop_infrastructure.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("VDI - Virtual Desktop Infrastructure")

    template_opts["product_virtual_desktop_infrastructure_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_virtual_desktop_infrastructure_01_header"
        ).content
    )

    template_opts["product_virtual_desktop_infrastructure_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_virtual_desktop_infrastructure_01_content"
        ).content
    )

    template_opts["product_virtual_desktop_infrastructure_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_virtual_desktop_infrastructure_02_header"
        ).content
    )

    template_opts["product_virtual_desktop_infrastructure_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_virtual_desktop_infrastructure_02_content"
        ).content
    )

    template_opts["product_virtual_desktop_infrastructure_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_virtual_desktop_infrastructure_03_header"
        ).content
    )

    template_opts["product_virtual_desktop_infrastructure_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_virtual_desktop_infrastructure_03_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def virtual_server(request):
    template = loader.get_template("django_company_cms_cloud/virtual_server.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Virtual Server")

    template_opts["product_virtual_server_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_virtual_server_01_header"
    ).content

    template_opts["product_virtual_server_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_virtual_server_01_content"
    ).content

    template_opts["product_virtual_server_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_virtual_server_02_header"
    ).content

    template_opts["product_virtual_server_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_virtual_server_02_content"
    ).content

    template_opts["product_virtual_server_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_virtual_server_03_header"
    ).content

    template_opts["product_virtual_server_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_virtual_server_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def network_management(request):
    template = loader.get_template("django_company_cms_cloud/network_management.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud")
    template_opts["content_title_sub"] = _("Network Management")

    template_opts["product_network_management_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_management_01_header"
        ).content
    )

    template_opts["product_network_management_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_management_01_content"
        ).content
    )

    template_opts["product_network_management_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_management_02_header"
        ).content
    )

    template_opts["product_network_management_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_management_02_content"
        ).content
    )

    template_opts["product_network_management_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_management_03_header"
        ).content
    )

    template_opts["product_network_management_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_network_management_03_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))
